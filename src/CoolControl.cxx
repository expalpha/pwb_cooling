#include <chrono>
#include <numeric>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <cmath>

#include "CoolControl.hh"

using namespace std::chrono;
using std::cout;
using std::cerr;
using std::endl;

CoolControl::CoolControl(bool midas):   binOut(4, HUBRELAY, SERNO_HUB0),
			      vOut(HUBVOUT, SERNO_HUB0),
			      freq_in(HUBFREQ_IN, SERNO_HUB1),
			      freq_out(HUBFREQ_OUT, SERNO_HUB1),
			      dispLCD(HUBDISPLAY, SERNO_HUB0),
			      dist(HUBDIST, SENSOR_TYPE_1101_SHARP_2Y0A21, SERNO_HUB1),
			      pres(HUBPRES, SENSOR_TYPE_1141, SERNO_HUB1),
			      temp(HUBTEMP, SERNO_HUB1)//,
			      // pwm(HUBPWM, SERNO_HUB0)
{
   is_midas = midas;
   verbose = !midas;
   cout << "Relay:  " << (binOut.AllGood()?"OK":"Fail") << endl;
   cout << "vOut:   " << (vOut.AllGood()?"OK":"Fail") << endl;
   cout << "freq_in:   " << (freq_in.AllGood()?"OK":"Fail") << endl;
   cout << "freq_out:   " << (freq_out.AllGood()?"OK":"Fail") << endl;
   cout << "dispLCD:" << (dispLCD.AllGood()?"OK":"Fail") << endl;
   cout << "dist:   " << (dist.AllGood()?"OK":"Fail") << endl;
   cout << "pres:   " << (pres.AllGood()?"OK":"Fail") << endl;
   cout << "temp:   " << (temp.AllGood()?"OK":"Fail") << endl;
   // cout << "pwm:   " << (pwm.AllGood()?"OK":"Fail") << endl;
   temp.SetType('K');
   freq_out.SetFreqCutoff(0.25);
   SetBacklight(0.1);
   dist.SetSensorValueChangeFunc(onLevelChange, (void*)this);
   pres.SetSensorValueChangeFunc(onPressureChange, (void*)this);
   temp.SetTempChangeFunc(onTempChange, (void*)this);
   freq_in.SetFrequencyChangeFunc(onFreqChange, (void*)this);
   freq_in.SetDataInterval(500);
   freq_out.SetFrequencyChangeFunc(onFreqChange, (void*)this);
   freq_out.SetDataInterval(500);
   dist.SetDataInterval(500);
   pres.SetDataInterval(500);
   temp.SetDataInterval(500);
   dist.SetSensorValueChangeTrigger(0);
   pres.SetSensorValueChangeTrigger(0);
   temp.SetTempChangeTrigger(0);
   pid.setOutputMin(0.);
   pid.setOutputMax(maxSpeed);
   pid.setClampI(true);
   pid.setErrorCoeff(1.);
   pid.setTAvg(5);
   pid.setSetPoint(setLevel);
   vOut.setLowerVoltageBound(0.);
   vOut.setUpperVoltageBound(5.);
   t0 = steady_clock::now();
   l = CalcLevel(dist.GetValue());
   p = pres.GetValue()*10.;
   PrintVals();
   fprintf(stderr, "CoolControl::ctor returns!\n");
}

void CoolControl::SetNAvg(int n){
   pq.resize(n);
   lq.resize(n);
   fqi.resize(n);
   fqo.resize(n);
   Tq.resize(n);
}

double CoolControl::CalcFlow(double freq, double factor){
   double f = freq * factor;
   if(f < 0.01) return 0.;
   return f;
}

double CoolControl::CalcLevel(double dist){
   if(dist <= 11. || dist != dist) l = 999.;
   else l = 395.9 - 11.2*dist;
   // if(verbose) cout << dist << " -> " << l << endl;
   return l;
}

void CCONV CoolControl::onLevelChange(PhidgetVoltageRatioInputHandle ch, void *ctx, double sensorValue, Phidget_UnitInfo *sensorUnit) {
   CoolControl *cc = (CoolControl*)ctx;
   cc->l = cc->CalcLevel(sensorValue);
   if(cc->l > cc->maxLevel){
      cc->runControl = false;
      cc->Vent();
      cerr << "Water level too high! Venting." << endl;
   }
   if(cc->is_midas)
      cc->lq_mid.push_back(cc->l);

   if(cc->lq.size()){
      cc->lq.push_back(cc->l);
      cc->lq.pop_front();
      cc->ml = std::accumulate(cc->lq.begin(), cc->lq.end(), 0.0)/double(cc->lq.size());
      // cc->pid.updateError(cc->ml);
   }// else {
   cc->pid.updateError(cc->l);
   // }
   cc->newVals = true;
   cc->PrintVals();


   cc->ControlStep();
}

void CCONV CoolControl::onPressureChange(PhidgetVoltageRatioInputHandle ch, void *ctx, double sensorValue,Phidget_UnitInfo *sensorUnit){
   CoolControl *cc = (CoolControl*)ctx;
   cc->p = 10.*sensorValue;
   if(cc->is_midas)
      cc->pq_mid.push_back(cc->p);

   if(cc->pq.size()){
      cc->pq.push_back(cc->p);
      cc->pq.pop_front();
      cc->mp = std::accumulate(cc->pq.begin(), cc->pq.end(), 0.0)/double(cc->pq.size());
   }
   cc->newVals = true;
   cc->PrintVals();
   cc->ControlStep();
}

void CCONV CoolControl::onFreqChange(PhidgetFrequencyCounterHandle ch, void *ctx, double sensorValue){
   int port;
   Phidget_getHubPort((PhidgetHandle)ch, &port);
   CoolControl *cc = (CoolControl*)ctx;
   switch(port){
   case HUBFREQ_IN:{
      cc->f_in = cc->CalcFlow(sensorValue, cc->flowFactor_in);
      if(cc->is_midas)
         cc->fqi_mid.push_back(cc->f_in);

      if(cc->fqi.size()){
         cc->fqi.push_back(cc->f_in);
         cc->fqi.pop_front();
         cc->mfi = std::accumulate(cc->fqi.begin(), cc->fqi.end(), 0.0)/double(cc->fqi.size());
      }
      break;
   }
   case HUBFREQ_OUT:{
      cc->f_out = cc->CalcFlow(sensorValue, cc->flowFactor_out);
      if(cc->is_midas)
         cc->fqo_mid.push_back(cc->f_out);

      if(cc->fqo.size()){
         cc->fqo.push_back(cc->f_out);
         cc->fqo.pop_front();
         cc->mfo = std::accumulate(cc->fqo.begin(), cc->fqo.end(), 0.0)/double(cc->fqo.size());
      }
      break;
   }
   default:
      cerr << "Unknown port " << port << " for flowmeter" << endl;
   }
   cc->newVals = true;
   cc->PrintVals();
}

void CCONV CoolControl::onTempChange(PhidgetTemperatureSensorHandle ch, void *ctx, double sensorValue){
   CoolControl *cc = (CoolControl*)ctx;
   cc->T = sensorValue;
   if(cc->is_midas)
      cc->Tq_mid.push_back(cc->T);

   if(cc->Tq.size()){
      cc->Tq.push_back(sensorValue);
      cc->Tq.pop_front();
   }
   cc->newVals = true;
   cc->PrintVals();
}

void CoolControl::PrintVals() {
   steady_clock::time_point t = steady_clock::now();
   static steady_clock::time_point tlast = t0;

   duration<double> dt = duration_cast<duration<double>>(t - tlast);
   if(dt.count() < t_refresh) return;
   tlast = t;

   if(verbose){
      duration<double> tnow = duration_cast<duration<double>>(t - t0);
      cout << tnow.count() << '\t' << p << '\t' << l << '\t' << f_in << '\t' << f_out;
      if(lq.size() > 1){
         cout << '\t' << mp << '\t' << ml << '\t' << mfi << '\t' << mfo;
      }
      cout << '\t' << vacState << '\t' << waterPumpSpeed;
      cout << endl;
   }

   std::ostringstream oss1;
   oss1 << "Pressure: " << std::setw(4) << int(p) << " mbar";
   dispLCD.Write(1, oss1.str().c_str());
   std::ostringstream oss2;
   oss2 << "Level:    " << std::setw(4) << int(l) << " mm";
   dispLCD.Write(2, oss2.str().c_str());
   std::ostringstream oss3;
   oss3 << "Flow_in:     " << std::setw(4) << std::setprecision(3) << f_in << " l/min";
   dispLCD.Write(3, oss3.str().c_str());
   std::ostringstream oss4;
   oss4 << "Flow_out:    " << std::setw(4) << std::setprecision(3) << f_out << " l/min";
   dispLCD.Write(4, oss4.str().c_str());
   std::ostringstream oss5;
   oss5 << "Temp.:    " << std::setw(4) << std::fixed << std::setprecision(1) << T << " C";
   dispLCD.Write(5, oss5.str().c_str());
   std::ostringstream oss6;
   oss6 << "Vac.: " << (binOut.GetState(REL_VAC)?"ON ":"OFF") << ", Pump: " << int(waterPumpSpeed+0.5) << "%";
   dispLCD.Write(6, oss6.str().c_str());
   newVals = false;
}

bool CoolControl::ToggleVac(){
   cout << "ToggleVac()" << endl;
   double s = binOut.GetState(REL_VAC);
   EngageControl(false);
   cout << "Previous state: " << s << endl;
   return SwitchVac(s==0);
}

bool CoolControl::ToggleWater(){
   cout << "ToggleWater()" << endl;
   double s = binOut.GetState(REL_WATER);
   EngageControl(false);
   cout << "Previous state: " << s << endl;
   return SwitchWPump(s==0);
}

bool CoolControl::ToggleVent(){
   cout << "ToggleVent()" << endl;
   double s = binOut.GetState(REL_VENT);
   EngageControl(false);
   cout << "Previous state: " << s << endl;
   return Vent(s==0);
}

bool CoolControl::SwitchVac(bool on){
   // cout << "Turn vac " << (on?"ON":"OFF") << endl;
   if(on){
      bool success = Vent(false);
      if(!success){          // close vent valve when turning on vac pump
         cerr << "Vent(off) didn't work" << endl;
         return false;
      }
   }
   bool success = (binOut.SetState(REL_VAC, on) == EPHIDGET_OK);
   if(success) vacState = on;
   return success;
}

bool CoolControl::SwitchWPump(bool on){
   int s1 = binOut.GetState(REL_WATER);
   bool success = (binOut.SetState(REL_WATER, on) == EPHIDGET_OK);
   int s2 = binOut.GetState(REL_WATER);
   if(verbose) cout << "WPump switch " << s1 << " -> " << s2 << " = " << int(success) << endl;
   return success;
}

bool CoolControl::Vent(bool vent){
   // cout << "Turn vent " << (vent?"ON":"OFF") << endl;
   if(vent){
      if(!SwitchVac(false)){     // turn off vac pump when venting
         cerr << "SwitchVac(off) didn't work" << endl;
         return false;
      }
   }
   return (binOut.SetState(REL_VENT, vent) == EPHIDGET_OK);
}

bool CoolControl::IncPump(int ds){
   cout << "IncPump()" << endl;
   double v = vOut.GetVoltage();
   int s = int(v/vMax * 100. + 0.5);
   return SetWaterPumpSpeed(s + ds);
}

bool CoolControl::SetWaterPumpSpeed(double s){
   static double oldfrac = 0.;
   if(s > maxSpeed) s = maxSpeed;
   double frac = double(s)/100.;

   // Adjust frac according to empirical calibration
   double modfrac;
   if(frac == 0. || frac == 1.){
      modfrac = frac;
   } else if(frac > oldfrac){
      modfrac = 0.89*frac + 0.0656;
   } else {
      modfrac = 0.9*frac + 0.0265;
   }

   // PWM controller is inverse to voltage, i.e. 0V -> 100%, 5V -> 0%
   double v = vMax * (1.0-modfrac);
   // cout << "SetVoltage " << v << endl;
   // if(vOut.SetVoltage(v) == EPHIDGET_OK && pwm.SetDutyCycle(frac) == EPHIDGET_OK){
   if(vOut.SetVoltage(v) == EPHIDGET_OK){
      // cout << "Water pump set to " << s << "%" << endl;
      waterPumpSpeed = s;
      oldfrac = frac;
      return true;
   } else {
      cerr << "Failed to set water pump speed: " << vOut.GetErrorCode() << ", " << endl;
      // cerr << "Failed to set water pump speed: " << vOut.GetErrorCode() << ", " << pwm.GetErrorCode() << endl;
      return false;
   }
}

bool CoolControl::ControlVac(){
   vacState = binOut.GetState(REL_VAC);
   bool prevState = vacState;
   if(l > maxLevel){
      // cout << "Level too high" << endl;
      vacState = false;
   } else {
      if(p > highPres){
         // cout << "p > " << highPres << endl;
         vacState = true;
      } else if(p < lowPres){
         // cout << "p < " << lowPres << endl;
         vacState = false;
      } else {
         // cout << "p is fine" << endl;
      }
   }
   // cout << "ControlVac: " << vacState << endl;
   if(vacState && !prevState)
      tvac = steady_clock::now();
   return SwitchVac(vacState);
}

bool CoolControl::ControlStep(){
   bool success = true;
   double pumpSpeed = pid.calcOutput();
   if(runControl){
      if(l > maxLevel){
         EngageControl(false);
         Vent(true);
         success = false;
      } else {
         success = ControlVac();
         success &= SetWaterPumpSpeed(pumpSpeed);
      }
   }
   return success;
}

void CoolControl::EngageControl(bool on, bool keep_settings){
   if(on)
      pid.ClearMemory();
   else if(!keep_settings){
      SetWaterPumpSpeed(0);
      SwitchVac(false);
   }
   runControl = on;
}

void CoolControl::SetLevel(double set){
   pid.setSetPoint(set);
   setLevel = set;
}

void CoolControl::SetMinLevel(double set){
   minLevel = set;
}

void CoolControl::SetMaxLevel(double set){
   maxLevel = set;
}

void CoolControl::SetPrimeLevel(double set){
   primeLevel = set;
}

void CoolControl::SetLoP(double set){
   lowPres = set;
}

void CoolControl::SetHiP(double set){
   highPres = set;
}

void CoolControl::SetMaxPumpSpeed(int set){
   maxSpeed = set;
}

void CoolControl::SetP(double pidP){
   pid.setErrorCoeff(pidP);
}
void CoolControl::SetI(double pidI){
   pid.setErrorIntCoeff(pidI);
}
void CoolControl::SetD(double pidD){
   pid.setErrorDiffCoeff(pidD);
}

void CoolControl::SetPidTAvg(double t){
   pid.setTAvg(t);
}

bool CoolControl::PrimePump(double t_prime){
   cout << "Priming water pump.." << endl;
   EngageControl(false);
   if(ml < primeLevel){
      cout << "Not enough water in vac. vessel (" << ml << " < " << primeLevel << "), switching on vac. pump." << endl;
      steady_clock::time_point t0 = steady_clock::now();
      steady_clock::time_point t = steady_clock::now();
      SwitchVac(true);
      while(duration_cast<duration<double>>(t - t0).count() < t_prime){
         if(ml > primeLevel) break;
         t = steady_clock::now();
      }
      if(ml < primeLevel){
         cerr << "Couldn't raise water level in vac. vessel!" << endl;
         SwitchVac(false);
         return false;
      }
   }
   cout << "Venting to reduce resistance." << endl;
   Vent();
   cout << "Switching on water pump." << endl;
   SwitchWPump(true);
   SetWaterPumpSpeed(maxSpeed);
   sleep(10);
   if(f_out < 1.){
      SwitchWPump(false);
      SetWaterPumpSpeed(0.);
      cerr << "No flow out of water pump!" << endl;
      return false;
   } else {
      cout << "Water flow seems to be OK: " << f_out << " l/min" << endl;
   }
   cout << "Turning water pump off, it's primed now." << endl;
   SetWaterPumpSpeed(0.);
   return true;
}

void CoolControl::SetBacklight(double bl){
   dispLCD.SetBacklight(bl);
}

CoolControl::vars_t CoolControl::GetVars(){
   vars_t v;
   if(pq_mid.size()){
      v.p = std::accumulate(pq_mid.begin(), pq_mid.end(), 0.0)/double(pq_mid.size());
      pq_mid.clear();
   }
   if(lq_mid.size()){
      v.l = std::accumulate(lq_mid.begin(), lq_mid.end(), 0.0)/double(lq_mid.size());
      lq_mid.clear();
   }
   if(fqi_mid.size()){
      v.f_in = std::accumulate(fqi_mid.begin(), fqi_mid.end(), 0.0)/double(fqi_mid.size());
      fqi_mid.clear();
   }
   if(fqo_mid.size()){
      v.f_out = std::accumulate(fqo_mid.begin(), fqo_mid.end(), 0.0)/double(fqo_mid.size());
      fqo_mid.clear();
   }
   if(Tq_mid.size()){
      v.T = std::accumulate(Tq_mid.begin(), Tq_mid.end(), 0.0)/double(Tq_mid.size());
      Tq_mid.clear();
   }
   v.speed = waterPumpSpeed;
   v.vac_state = vacState;
   return v;
}

bool CoolControl::AllGood(){
   if(!binOut.AllGood()) std::cerr << "Relay problem!" << std::endl;
   if(!vOut.AllGood()) std::cerr << "DAC problem!" << std::endl;
   if(!freq_in.AllGood()) std::cerr << "InFlow problem!" << std::endl;
   if(!freq_out.AllGood()) std::cerr << "OutFlow problem!" << std::endl;
   if(!dispLCD.AllGood()) std::cerr << "Display problem!" << std::endl;
   if(!dist.AllGood()) std::cerr << "Level problem!" << std::endl;
   if(!pres.AllGood()) std::cerr << "Pressure problem!" << std::endl;
   if(!temp.AllGood()) std::cerr << "Temp problem!" << std::endl;
   return (binOut.AllGood() &&
           vOut.AllGood() &&
           freq_in.AllGood() &&
           freq_out.AllGood() &&
           dispLCD.AllGood() &&
           dist.AllGood() &&
           pres.AllGood() &&
           temp.AllGood());
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
