#ifndef COOLCONTROL_H
#define COOLCONTROL_H

#include "VoltageOutput.hh"
#include "VoltageRatioSensor.hh"
#include "RelayArray.hh"
#include "DisplayLCD.hh"
#include "FreqCounter.hh"
#include "Thermocouple.hh"
#include "DigitalOutput.hh"
#include "PIDinstance.hh"

#include <chrono>
#include <deque>

#define SERNO_HUB0 559911
#define SERNO_HUB1 561109

#define HUBRELAY 0
#define HUBVOUT 1
#define HUBDISPLAY 2
#define HUBFREQ_IN 0
#define HUBFREQ_OUT 1
#define HUBDIST 4
#define HUBPRES 3
#define HUBPWM 5

#define HUBTEMP 2

#define REL_WATER 3
#define REL_VAC 1
#define REL_VENT 0

using namespace std::chrono;

class CoolControl
{
public:
   /// Instantiates all relevant Phidgets
   CoolControl(bool midas = false);

   /// Toggle vacuum pump state
   bool ToggleVac();

   /// Toggle water pump state
   bool ToggleWater();

   /// Toggle vent valve
   bool ToggleVent();

   /// Switch vacuum pump on/off
   bool SwitchVac(bool on);

   /*! \brief Switch water pump on/off
    *
    * Should only be used for emergencies, otherwise leave on and set speed to zero
    */
   bool SwitchWPump(bool on);

   /// Turn off vac. pump and vent vessel, argument false to close vent valve
   bool Vent(bool vent = true);

   /// Modify water pump speed by given increment
   bool IncPump(int ds);

   /// Set water pump speed to s%
   bool SetWaterPumpSpeed(double s);

   /// Set number of readings to average over for display
   void SetNAvg(int n);

   /// Set water level setpoint
   void SetLevel(double set);

   /// Set minimum water level
   void SetMinLevel(double set);

   /// Set maximum water level
   void SetMaxLevel(double set);

   /// Set water level required for priming the pump
   void SetPrimeLevel(double set);

   /// Set pressure at which the vacuum pump turns on
   void SetHiP(double set);

   /// Set pressure at which the vacuum pump turns on
   void SetLoP(double set);

   /// Set maximum water pump speed in percent
   void SetMaxPumpSpeed(int set);

   /// Turn PID control on/off
   void EngageControl(bool on, bool keep_settings = false);

   /// Set PID P-parameter
   void SetP(double pidP);
   /// Set PID I-parameter
   void SetI(double pidI);
   /// Set PID D-parameter
   void SetD(double pidD);

   /// Set smoothing time for level reading
   void SetPidTAvg(double t);

   /// Procedure to ensure water pump is filled with water
   bool PrimePump(double t_prime = 300.);

   /// Set screen brightness
   void SetBacklight(double bl = 0.7);

   struct vars_t {
      double p = 0.;
      double l = 0.;
      double f_in = 0.;
      double f_out = 0.;
      double T = 0.;
      double speed = 0.;
      bool vac_state = false;
   };

   vars_t GetVars();

   void SetVerbose(bool v){ verbose = v; }

   double GetPres(){ return p; }
   double GetLevel(){ return l; }

   bool AllGood();
private:
   /// Print diagnostics to stdout and LCD display
   void PrintVals();
   double CalcFlow(double freq, double factor = 1.0);
   double CalcLevel(double dist);

   bool ControlVac();
   bool ControlStep();

   /// Callback for water level change
   static void CCONV onLevelChange(PhidgetVoltageRatioInputHandle ch, void *ctx, double sensorValue, Phidget_UnitInfo *sensorUnit);

   /// Callback for vacuum pressure change
   static void CCONV onPressureChange(PhidgetVoltageRatioInputHandle ch, void *ctx, double sensorValue,Phidget_UnitInfo *sensorUnit);

   /// Callback for water flow change
   static void CCONV onFreqChange(PhidgetFrequencyCounterHandle ch, void *ctx, double sensorValue);

   /// Callback for water temperature change
   static void CCONV onTempChange(PhidgetTemperatureSensorHandle ch, void *ctx, double sensorValue);

   PhidgetPP::RelayArray binOut;		///< Relay phidget controlling pump power and solenoid valves
   PhidgetPP::VoltageOutput vOut;		///< Voltage output phidget controlling water pump speed
   PhidgetPP::FreqCounter freq_in, freq_out;		///< Frequency counter measuring water flow
   PhidgetPP::DisplayLCD dispLCD;		///< LCD display on control box frontpanel
   PhidgetPP::VoltageRatioSensor dist;	///< IR distance sensor for water level measurement
   PhidgetPP::VoltageRatioSensor pres;	///< Vacuum pressure sensor
   PhidgetPP::Thermocouple temp;		///< K thermocouple in water reservoir tank
   // DigitalOutput pwm;          ///< Direct PWM control for water pump

   double p = 0.;
   double l = 0.;
   double f_in = 0.;
   double f_out = 0.;
   double T = 0.;
   steady_clock::time_point t0, tvac;

   const static bool fVerbose = true;
   bool newVals = true;
   float t_refresh = 1.;

   std::deque<double> pq, lq, fqi, fqo, Tq;
   std::vector<double> pq_mid, lq_mid, fqi_mid, fqo_mid, Tq_mid;

   double minLevel = 60.;      // Level monitor bottoms out around 50
   double primeLevel = 200.;
   double maxLevel = 260.;
   double lowPres = 200.;
   double highPres = 300.;
   double setLevel = 200.;

   double mp, ml, mfi, mfo;

   bool runControl = false;

   bool vacState = false;
   double waterPumpSpeed;

   int maxSpeed = 75;

   const double vMax = 5.;

   const double tvaclim = 60.;  // PID control paused during this time after turning on vac

   const double flowFactor_in = 0.12; // relative flowmeter calibration, change if changing flowmeters
   const double flowFactor_out = 1./21.; // relative flowmeter calibration, change if changing flowmeters

   PIDinstance pid;

   bool is_midas;
   bool verbose = false;
   };
#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
