#include <iostream>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <deque>
#include <numeric>

using namespace std;
using namespace std::chrono;

class PIDinstance{
    /*! \brief Class for a PID system
     *
     *Class takes in coefficients for a PID system, can be manually updated and return output values
     */
public:
    /*! \brief Constructor
     *
     * Constructor takes in coefficients of PID and the set point
     */
    PIDinstance(double P_coeff = 1.0,double I_coeff = 0.,double D_coeff = 0.,double setPoint_def = 300.){
        errorCoeff = P_coeff;
        errorIntCoeff = I_coeff;
        errorDiffCoeff = D_coeff;
        setPoint = setPoint_def;
        error = 0;
        errorDiff = 0;
        errorInt = 0;
        output = 0;
        time0 = steady_clock::now();
        logfile.open("pidlogfile.csv");
	logfile << "# PID " << errorCoeff << '\t' << errorIntCoeff << '\t' << errorDiffCoeff << endl;
        logfile << fixed << setprecision(3);
        logfile << "Time,Error,Integral,Derivative,Output\n";
    }

    /*! \brief Destructor
     *
     *Closes the file logging the error,integral,derivative and output
     */
    ~PIDinstance(){
        logfile.close();
    }

    /*! \brief Pass new reading to PIDinstance and calculate error
     *
     * calculates the direct (P) error
     */
    int updateError(double new_value){
        times.push_back((duration_cast<duration<double>>(steady_clock::now()-time0)).count());
        errors.push_back(new_value - setPoint);
        if(times.size()){
            while(times.size() && times.back() - times.front() > t_avg){
                times.pop_front();
                errors.pop_front();
            }
        }
        if(times.size()){
            prevError = error;
            prevTime = time;
            time = std::accumulate(times.begin(), times.end(), 0.0)/times.size();
            error = std::accumulate(errors.begin(), errors.end(), 0.0)/errors.size();
        }
        return 0;
    }

    /// Calculates the integral of the error
    int calcInt(){
        if(prevTime == 0) return 0;
        if(clampI){ // Wind-up protection "Clamping"
	  if((output >= outputMax && error > 0.) ||
	     (output <= outputMin && error < 0.))
	    return 0;
	}
        double dt = time - prevTime;
        errorInt += dt*(error + prevError)/2;
        return 0;
    }

    /// Calculates the derivative of the error
    int calcDiff(){
        if(prevTime > 0 && time != prevTime){
            double dt = time - prevTime;
            errorDiff = (error-prevError)/dt;
        }
        else{
            errorDiff = 0;
        }
        return 0;
    }

    /// Calculates the output of the system by adding PID terms
    double calcOutput(){
        calcInt();
        calcDiff();
        output = errorCoeff*error + errorIntCoeff*errorInt + errorDiffCoeff*errorDiff;
        if(outputBoundMin){
            if(output < outputMin){
                output = outputMin;
            }
        };
        if(outputBoundMax){
            if(output > outputMax){
                output = outputMax;
            }
        };
        logCSV();
        return output;
    }

    /// Sets the maximum output value
    int setOutputMax(double newMax){
        outputBoundMax = true;
        outputMax = newMax;
        return 0;
    }

    /// Returns the maximum output value
    double getOutputMax(){
        if(outputBoundMax){
            return outputMax;
        }
        else{
            std::cout << "No output max";
            return 99999;
        }
    }

    /// Removes the upper bound for the output
    int removeOutputMax(){
        outputBoundMax = false;
        return 0;
    }

    /// Sets the minimum output value
    int setOutputMin(double newMin){
        outputBoundMin = true;
        outputMin = newMin;
        return 0;
    }

    /// Returns the minimum output value
    double getOutputMin(){
        if(outputBoundMin){
            return outputMin;
        }
        else{
            std::cout << "No output min";
            return -99999;
        }
    }

    /// Removes the lower bound for the output
    int removeOutputMin(){
        outputBoundMin = false;
        return 0;
    }

    /// Sets the target value for the input quantity
    int setSetPoint(double new_set_point){
        setPoint = new_set_point;
	logfile << "# new setpoint " << setPoint << endl;
        return 0;
    }

    /// Returns the current setpoint
    double getSetPoint(double new_set_point){
        return setPoint;
    }

    /*! \brief Set P coefficient
     *
     * sets the coefficient for the P term
     */
    int setErrorCoeff(double new_coeff){
        errorCoeff = new_coeff;
	logfile << "# new PID " << errorCoeff << '\t' << errorIntCoeff << '\t' << errorDiffCoeff << endl;
        return 0;
    }

    /// Returns the cofficient for the P term
    double getErrorCoeff(double new_coeff){
        return errorCoeff;
    }

    /*! \brief Set I coefficient
     *
     * sets the coeffficient for the I terms
     */
    int setErrorIntCoeff(double new_coeff){
        errorIntCoeff = new_coeff;
	logfile << "# new PID " << errorCoeff << '\t' << errorIntCoeff << '\t' << errorDiffCoeff << endl;
        return 0;
    }

    /// Returns the coefficient for the I term
    double getErrorIntCoeff(double new_coeff){
        return errorIntCoeff;
    }

    /*! \brief Set D coefficient
     *
     * sets the coefficient for the D term
     */
    int setErrorDiffCoeff(double new_coeff){
        errorDiffCoeff = new_coeff;
	logfile << "# new PID " << errorCoeff << '\t' << errorIntCoeff << '\t' << errorDiffCoeff << endl;
        return 0;
    }

    /// Returns the coefficient for the D term
    double getErrorDiffCoeff(double new_coeff){
        return errorDiffCoeff;
    }

    /// Read back current error value
    double getError(){
        return error;
    }

    /*! \brief Writes csv file for PID tuning
     *
     * writes the values of time, error, integral of error, derivative of error and output values
     */
    int logCSV(){
        logfile << time << ",\t";
        logfile << error<< ",\t";
        logfile << errorInt<< ",\t";
        logfile << errorDiff<< ",\t";
        logfile << output<< endl;
        return 0;
    }

    /*! \brief Turn I-term clamping on/off
     *
     * in order to prevent "I-wind-up" error integral is not accumulated when output is already at maximum value
     */
    bool setClampI(bool clamp){
      clampI = clamp;
      return clampI;
    }

    /// Set smoothing time for process variable
    void setTAvg(double t){
      if(t <= 0){
	cerr << "t_avg must be positive" << endl;
      }
      t_avg = t;
    }

    /// Reset error values to allow fresh start of PID
    void ClearMemory(){
      time0 = steady_clock::now();
      times.clear();
      errors.clear();
      error = 0.;
      time = 0.;
      prevTime = 0.;
      prevError = 0.;
      errorInt = 0.;
    }

private:
    steady_clock::time_point time0;
    double time = 0.;
    double prevTime = 0.;
    double setPoint;
    double error = 0.;
    double prevError = 0.;
    double errorInt;
    double errorDiff;
    double errorCoeff;
    double errorIntCoeff;
    double errorDiffCoeff;
    double output;
    bool outputBoundMin = false;
    bool outputBoundMax = false;
    double outputMin, outputMax;
    ofstream logfile;
    std::deque<double> times;
    std::deque<double> errors;

    bool clampI = false;
    double t_avg = 5.0;
};
