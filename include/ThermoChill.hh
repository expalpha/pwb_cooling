



#ifndef THERMOCHILL_H
#define THERMOCHILL_H
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <cassert>
#include <vector>
#include <string>

class ThermoChill
/*! \brief Class to communicate with ThermoChill chiller via ttyUSB
*
* Should communicate with connected chiller, provided it's in remote mode.
*/
{
public:
  ThermoChill();
  ~ThermoChill();

  /*! \brief Connect to chiller via ttyUSB
  *
  * @return 0 on success, >0 otherwise
  */
  int Connect();
  bool CheckConnection(); ///< Handshake with chiller
  bool SetTemp(int16_t tset); ///< Set chiller demand temperature
  double ReadTemp();  ///< Read back chiller temperature
  bool SwitchChiller(bool on); ///< Start/stop chilling

  /*! \brief Request status message from chiller
  *
  * Status  interpretation table:
  * BIT | byte 0 | byte 1
  * ----|--------|--------
  * b.7 | External Temp Sensor Enabled | Reserved
  * b.6 | Level 1 Warning/Auto Refill  | Reserved
  * b.5 | Low Flow Warning             | Internal Temp Sensor Fault
  * b.4 | Low Level 2 Warning          | External Temp Sensor Fault
  * b.3 | High or Low Temp Warning     | High Temp/Fixed High Temp Fault
  * b.2 | High or Low Temp Bypass      | Low Temp/Fixed Low Temp Fault
  * b.1 | Chiller Faulted*             | Low Flow Fault
  * b.0 | Chiller Running*             | Low Level 2 Fault
  * 
  * Byte 0 bits 0 and 1 are mutually exclusive, Byte 1 only contains fault details if "Chiller Faulted" is set
  */
  int CheckStatus();
  uint8_t GetWarnings();  ///< Extract top 6 bits of byte 0 of the status
  bool connected = false; ///< *true* if chiller connection is up and running
private:
  struct termios tioTC;
  int ttyTC;
  bool verbose = true;
  uint8_t d1 = 0;
  std::vector<std::string> FindTtyUSB();  ///< Generate list of candidate ttyUSB devices
  int Exchange(uint8_t cmd, int16_t &value);  ///< Exchange message with chiller
  int16_t GetStatus();
  int SetTermAttr();  ///< Set correct RS232 values for chiller
};

#endif
