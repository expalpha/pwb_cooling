#include "CoolControl.hh"
#include <iostream>
#include <queue>
#include <string>
#include <unistd.h>

using std::cerr;
using std::cout;
using std::endl;
using std::queue;
using std::string;

enum command
{
    vent,
    drain,
    prime,
    evac,
    seal,
    wait
};

void usage()
{
    cout << "Manual control for ALPHAg water cooling system" << endl;
    cout << "Usage: manualControl <command 1 [arg 1]> [command 2 [arg 2]]..." << endl;
    cout << "Commands will we performed in order. Valid commands:" << endl;
    cout << "vent <target pressure>\topen vent valve and vent until target pressure reached, zero to vent until manual change" << endl;
    cout << "evac <target pressure>\tpump down to target pressure reached, zero to pump until manual change" << endl;
    cout << "drain <target level>\tturn on water pump to drain water from vac. vessel, zero to pump until end of program" << endl;
    cout << "seal                \tclose vent valve" << endl;
    cout << "wait <minutes>\twait before progressing to next command" << endl;
    cout << "prime               \tperform pump priming procedure" << endl;
}

int main(int argc, char **argv)
{
    CoolControl cooling;
    cooling.SetVerbose(false);
    cooling.SetNAvg(10);
    cooling.SetWaterPumpSpeed(0);

    queue<command> cmdqueue;
    queue<double> argsqueue;

    if (argc == 1)
    {
        usage();
        return 0;
    }

    for (int i = 1; i < argc; i++)
    {
        string cmd(argv[i]);
        if (cmd == "prime")
            cmdqueue.push(prime);
        else if (cmd == "wait")
        {
            cmdqueue.push(wait);
            argsqueue.push(strtod(argv[++i], nullptr));
        }
        else if (cmd == "drain")
        {
            cmdqueue.push(drain);
            argsqueue.push(strtod(argv[++i], nullptr));
        }
        else if (cmd == "vent")
        {
            cmdqueue.push(vent);
            argsqueue.push(strtod(argv[++i], nullptr));
        }
        else if (cmd == "evac")
        {
            cmdqueue.push(evac);
            argsqueue.push(strtod(argv[++i], nullptr));
        }
        else if (cmd == "seal")
        {
            cmdqueue.push(seal);
        }
        else
        {
            cout << "Skipping unknown command " << cmd << endl;
        }
    }

    while (!cmdqueue.empty())
    {
        command c = cmdqueue.front();
        cmdqueue.pop();
        switch (c)
        {
        case evac:
        {
            double p = argsqueue.front();
            argsqueue.pop();
            cout << "Pumping down" << endl;
            cooling.SwitchVac(true);
            if (p <= 0)
                break;
            else
            {
                double pp = cooling.GetPres();
                while (pp > p)
                {
                    cout << pp << " > " << p << endl;
                    sleep(1);
                    pp = cooling.GetPres();
                }
                cout << "Target pressure reached." << endl;
                cooling.SwitchVac(false);
            }
            break;
        }
        case vent:
        {
            double p = argsqueue.front();
            argsqueue.pop();
            cout << "Venting" << endl;
            cooling.Vent(true);
            if (p <= 0)
                break;
            else
            {
                double pp = cooling.GetPres();
                while (pp < p)
                {
                    cout << pp << " < " << p << endl;
                    sleep(1);
                    pp = cooling.GetPres();
                }
                cout << "Target pressure reached." << endl;
                cooling.Vent(false);
            }
            break;
        }
        case drain:
        {
            double l = argsqueue.front();
            argsqueue.pop();
            cout << "Pumping water from vac. vessel" << endl;
            cooling.SwitchWPump(true);
            cooling.SetWaterPumpSpeed(50);
            if (l <= 0)
                break;
            else
            {
                double ll = cooling.GetLevel();
                if (ll < 0)
                    ll = 999.;
                while (ll > l)
                {
                    cout << ll << " > " << l << endl;
                    sleep(1);
                    ll = cooling.GetLevel();
                }
                cout << "Target level reached." << endl;
                cooling.SetWaterPumpSpeed(0);
            }
            break;
        }
        case seal:
            cout << "Closing vent" << endl;
            cooling.Vent(false);
            break;
        case prime:
            cout << "Priming pump..." << endl;
            cooling.PrimePump();
            break;
        case wait:
        {
            double t = argsqueue.front();
            argsqueue.pop();
            cout << "Waiting " << t << " minutes" << endl;
            sleep(60 * t);
            break;
        }
        }
    }
    return 0;
}
