/*******************************************************************\

  Name:         tmfe_example_everything.cxx
  Created by:   K.Olchanski

  Contents:     Example Front end to demonstrate all functions of TMFE class

\********************************************************************/

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <math.h> // M_PI

#include "midas.h"
#include "tmfe.h"
#include "CoolControl.hh"
#include "ThermoChill.hh"
#include <iostream>
void cool_callback(INT hDB, INT hkey, INT index, void *feptr);

/// Midas equipment for the cooling system
class CoolEq :
   public TMFeEquipment
{
public:
   CoolEq(const char* eqname, const char* eqfilename) // ctor
      : TMFeEquipment(eqname, eqfilename), cc(true)
   {
      printf("CoolEq::ctor!\n");

      // configure the equipment here:
      printf("CoolEq::ctor!\n");
      if(!cc.AllGood()){
         printf("Something went wrong.\n");
         exit(FE_ERR_HW);
      }
      cc.SetWaterPumpSpeed(0);
      cc.SwitchWPump(true);
      cc.SetNAvg(5);
      if(tc.Connect()){
         printf("ThermoChill connection failed, will run without.\n");
      }

      //fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 1;
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = true;
      fEqConfEnablePoll = true; // enable polled equipment
      //fEqConfPollSleepSec = 0; // to create a "100% CPU busy" polling loop, set poll sleep time to zero
   }

   ~CoolEq() // dtor
   {
      printf("CoolEq::dtor!\n");
      cc.EngageControl(false);
      cc.SetWaterPumpSpeed(0);
      cc.SwitchWPump(false);
      cc.SwitchVac(false);
      // Vent(5);
   }

   void HandleUsage()
   {
      printf("CoolEq::HandleUsage!\n");
   }

   TMFeResult HandleInit(const std::vector<std::string>& args)
   {
      printf("CoolEq::HandleInit!\n");
      fMfe->DeregisterTransitions();
      fEqConfReadOnlyWhenRunning = false; // overwrite ODB Common RO_RUNNING to false
      fEqConfWriteEventsToOdb = true; // overwrite ODB Common RO_ODB to true

      double dummy;
      bool dummyB = false;
      fOdbEqVariables->RD("pressure", &dummy, true);
      fOdbEqVariables->RD("level", &dummy, true);
      fOdbEqVariables->RD("in_flow", &dummy, true);
      fOdbEqVariables->RD("out_flow", &dummy, true);
      fOdbEqVariables->RD("temperature", &dummy, true);
      fOdbEqVariables->RD("pump_speed", &dummy, true);
      fOdbEqVariables->RB("vac_pump", &dummyB, true);

      fOdbEqVariables->RD("T_chiller", &t_chill, true);
      fOdbEqVariables->RB("chiller_ok", &tc_ok, true);
      bool tc_connected = tc.connected;
      fOdbEqVariables->RB("chiller_connected", &tc_connected, true);
      fOdbEqVariables->RI("chiller_warnings", &tc_warnings, true);
      fOdbEqSettings->RI("T_chiller", &t_chill_set, true);
      if(tc.connected) tc.SetTemp(t_chill_set);

      fOdbEqSettings->RD("set_level", &set_level, true);
      cc.SetLevel(set_level);
      fOdbEqSettings->RD("p_low", &p_low, true);
      cc.SetLoP(p_low);
      fOdbEqSettings->RD("p_high", &p_high, true);
      cc.SetHiP(p_high);
      fOdbEqSettings->RB("PID_on", &pid_on, true);
      pid_on = false;
      fOdbEqSettings->WB("PID_on", pid_on);
      fOdbEqSettings->RD("t_prime", &t_prime, true);
      fOdbEqSettings->RB("prime_pump", &prime_pump, true);
      prime_pump = false;
      fOdbEqSettings->WB("prime_pump", prime_pump);

      odb_limits = fOdbEqSettings->Chdir("Limits", true);
      odb_limits->RD("min_level", &min_level, true);
      cc.SetMinLevel(min_level);
      odb_limits->RD("max_level", &max_level, true);
      cc.SetMaxLevel(max_level);
      odb_limits->RD("prime_level", &prime_level, true);
      cc.SetPrimeLevel(prime_level);
      odb_limits->RI("max_pump", &max_pump, true);
      cc.SetMaxPumpSpeed(max_pump);

      odb_pid = fOdbEqSettings->Chdir("PID parameters", true);
      odb_pid->RD("P", &pid_p, true);
      cc.SetP(pid_p);
      odb_pid->RD("I", &pid_i, true);
      cc.SetI(pid_i);
      odb_pid->RD("D", &pid_d, true);
      cc.SetD(pid_d);
      odb_pid->RD("time", &pid_t, true);
      cc.SetPidTAvg(pid_t);

      char tmpbuf[80] = "/Equipment/CoolControl/Settings/";
      printf("Prog name = %s\n", fMfe->fProgramName.c_str()); 
      // sprintf(tmpbuf, "/Equipment/%s/Settings/", fMfe->fProgramName.c_str());
      HNDLE hkey;
      if(db_find_link(fMfe->fDB, 0, tmpbuf, &hkey) == DB_SUCCESS){
         KEY key;
         db_get_key(fMfe->fDB, hkey, &key);
         db_watch(fMfe->fDB, hkey, cool_callback, (void*)this);
         printf("Hotlink on for key %s\n", key.name);
      } else {
         printf("Can't watch directory\n");
      }

      odb_status = fOdbEqSettings->Chdir("../Status", true);
      odb_status->RB("PID_on", &rpid_on, true);
      odb_status->RB("Priming", &rpriming, true);
      odb_status->RB("Good", &good, true);
      odb_status->RS("State", &rstate, true);

      EqSetStatus("Started...", "white");
      //EqStartPollThread();
      return TMFeOk();
   }

   TMFeResult HandleRpc(const char* cmd, const char* args, std::string& response)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);

      // RPC handler
      std::string cmdstr(cmd);

      if(cmdstr == std::string("prime")){
         prime_pump = true;
         fOdbEqSettings->WB("prime_pump", prime_pump);
         response = "OK";
         return TMFeOk();
      } else if(cmdstr == std::string("vent")){
         prime_pump = false;
         fOdbEqSettings->WB("pid_on", false);
         if(cc.Vent())
            response = "OK";
         else
            response = "Fail";
         return TMFeOk();
      } else if(cmdstr == std::string("vac")){
         prime_pump = false;
         ignore_pid = true;
         fOdbEqSettings->WB("pid_on", false);
         // fMfe->PollMidas(500);
         usleep(100000);
         if(cc.ToggleVac())
            response = "OK";
         else
            response = "Fail";
         return TMFeOk();
      } else if(cmdstr == std::string("water")){
         prime_pump = false;
         ignore_pid = true;
         fOdbEqSettings->WB("pid_on", false);
         // fMfe->PollMidas(500);
         usleep(100000);
         int speed = atoi(args);
         if(cc.SetWaterPumpSpeed(speed)){
            response = "OK";
         } else
            response = "Fail";
         return TMFeOk();
      }

      return TMFeOk();
   }

   void HandlePeriodic()
   {
      printf("CoolEq::HandlePeriodic!\n");
      // double t = TMFE::GetTime();
      CoolControl::vars_t vars = cc.GetVars();
      fOdbEqVariables->WD("pressure", vars.p);
      fOdbEqVariables->WD("level", vars.l);
      fOdbEqVariables->WD("in_flow", vars.f_in);
      fOdbEqVariables->WD("out_flow", vars.f_out);
      fOdbEqVariables->WD("temperature", vars.T);
      fOdbEqVariables->WD("pump_speed", vars.speed);
      fOdbEqVariables->WB("vac_pump", vars.vac_state);
      // double data = 100.0*sin(0*M_PI/2.0+M_PI*t/40);
      // SendData(data);

      bool tc_connected = tc.CheckConnection();
      if(!tc_connected){
         tc_connected = (tc.Connect()==0);
      }
      if(tc_connected){
         int status = tc.CheckStatus();
         if(status == 0xE){
            tc.SwitchChiller(true);
         }
         status = tc.CheckStatus();
         if(status) cerr << "Chiller status: " << status << endl;
         tc_ok = (status == 0);
      } else {
         tc_ok = false;
         static bool first = true;
         if(first)
            fMfe->Msg(MERROR, "HandlePeriodic", "Chiller connection error!");
         first = false;
         return;
      }
      
      tc_warnings = tc.GetWarnings();
      fOdbEqVariables->WI("chiller_warnings", tc_warnings);
      if(tc_warnings){
         fMfe->Msg(MERROR, "HandlePeriodic", "Chiller warnings! %x", tc_warnings);
      }
      t_chill = tc.ReadTemp();
      fOdbEqVariables->WD("T_chiller", t_chill);

      char status_buf[256];
      sprintf(status_buf, "Ctrl %s, p = %.0f mbar, l = %.0f mm, flow = %.1f l/min", (pid_on?"ON":"OFF"), vars.p, vars.l, vars.f_in);
      EqSetStatus(status_buf, (pid_on?"#00FF00":"#00FFFF"));
   }

   void fecallback(HNDLE hkey, INT index){
      KEY key;
      int status = db_get_key(fMfe->fDB, hkey, &key);
      if(status == DB_SUCCESS){
         printf("ODB entry %s changed!\n", key.name);
      } else {
         printf("ODB error!\n");
         return;
      }
      std::string name(key.name);
      if(name == std::string("set_level")){
         fOdbEqSettings->RD(name.c_str(), &set_level);
         cc.SetLevel(set_level);
      } else if(name == std::string("p_low")){
         fOdbEqSettings->RD(name.c_str(), &p_low);
         cc.SetLoP(p_low);
      } else if(name == std::string("p_high")){
         fOdbEqSettings->RD(name.c_str(), &p_high);
         cc.SetHiP(p_high);
      } else if(name == std::string("min_level")){
         odb_limits->RD(name.c_str(), &min_level);
         cc.SetMinLevel(min_level);
      } else if(name == std::string("max_level")){
         odb_limits->RD(name.c_str(), &max_level);
         cc.SetMaxLevel(max_level);
      } else if(name == std::string("prime_level")){
         odb_limits->RD(name.c_str(), &prime_level);
         cc.SetPrimeLevel(prime_level);
      } else if(name == std::string("t_prime")){
         odb_limits->RD(name.c_str(), &t_prime);
      } else if(name == std::string("max_pump")){
         odb_limits->RI(name.c_str(), &max_pump);
         cc.SetMaxPumpSpeed(max_pump);
      } else if(name == std::string("P")){
         odb_pid->RD(name.c_str(), &pid_p);
         cc.SetP(pid_p);
      } else if(name == std::string("I")){
         odb_pid->RD(name.c_str(), &pid_i);
         cc.SetI(pid_i);
      } else if(name == std::string("D")){
         odb_pid->RD(name.c_str(), &pid_d);
         cc.SetD(pid_d);
      } else if(name == std::string("time")){
         odb_pid->RD(name.c_str(), &pid_t);
         cc.SetPidTAvg(pid_t);
      } else if(name == std::string("PID_on")){
         fOdbEqSettings->RB(name.c_str(), &pid_on);
         if(pid_on){
            rstate = "PID controlled";
         } else {
            rstate = "Idle";
         }
         cc.EngageControl(pid_on, ignore_pid);
         ignore_pid = false;
      } else if(name == std::string("prime_pump")){
         fOdbEqSettings->RB(name.c_str(), &prime_pump);
         if(prime_pump){
            fOdbEqSettings->WB("PID_on", false);
            rstate = "Priming";
            rpriming = true;
            // fMfe->Msg(MINFO, "fecallback", "Priming water pump, wait %f seconds", t_prime);
            fMfe->Msg(MERROR, "fecallback", "Priming water pump not implemented");
            // FIXME: This must not hold up the thread, need to change how it works.
            // if(!cc.PrimePump()){
            //    fMfe->Msg(MERROR, "fecallback", "Priming water pump failed");
            //    good = false;
            // } else {
            //    fMfe->Msg(MINFO, "fecallback", "Water pump primed, ready to go");
            // }
         } else {
            rpriming = false;

         }
      } else if(name == std::string("T_chiller")){
         fOdbEqSettings->RI(name.c_str(), &t_chill_set);
         if(tc.connected) tc.SetTemp(t_chill_set);
      } else {
         fMfe->Msg(MINFO, "fecallback", "Not sure what to do with ODB entry %s", name.c_str());
      }
   }

   void Vent(int seconds = 30){
      cc.SetVerbose(true);
      cc.EngageControl(false);
      cc.SetWaterPumpSpeed(0);
      cc.Vent(true);
      for(int i = 0; i < seconds; i++){
         sleep(1);
         HandlePeriodic();
      }
   }

private:
   CoolControl cc;
   ThermoChill tc;
   MVOdb *odb_limits, *odb_pid, *odb_status;

   double set_level = 300.;
   double p_low = 200.;
   double p_high = 300.;
   double min_level = 100.;
   double max_level = 450.;
   double prime_level = 200.;
   double t_prime = 300.;
   int max_pump = 75;
   double pid_p = 1.;
   double pid_i = 0.01;
   double pid_d = 0.;
   double pid_t = 5.;
   bool pid_on = false;
   bool ignore_pid = false;     // on next change in pid_on variable, do not change pump settings
   bool prime_pump = false;

   int t_chill_set = 16;
   double t_chill = 0.;
   int tc_warnings = 0;
   bool tc_ok = false;

   bool rpid_on = false;
   bool rpriming = false;
   bool good = true;
   std::string rstate = "Idle";
};

void cool_callback(INT hDB, INT hkey, INT index, void *feptr)
{
   CoolEq* fe = (CoolEq*)feptr;
   fe->fecallback(hkey, index);
}

// example frontend

class FeCooling: public TMFrontend
{
public:
   FeCooling() // ctor
   {
      printf("FeCooling::ctor!\n");
      FeSetName("feCooling");
      CoolEq *eq = new CoolEq("CoolControl", __FILE__);
      eq->fEqConfBuffer.clear();
      FeAddEquipment(new CoolEq("CoolControl", __FILE__));
   }

   void HandleUsage()
   {
      printf("FeCooling::HandleUsage!\n");
   };

   TMFeResult HandleArguments(const std::vector<std::string>& args)
   {
      printf("FeCooling::HandleArguments!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendInit(const std::vector<std::string>& args)
   {
      printf("FeCooling::HandleFrontendInit!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendReady(const std::vector<std::string>& args)
   {
      printf("FeCooling::HandleFrontendReady!\n");
      //FeStartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };

   void HandleFrontendExit()
   {
      printf("FeCooling::HandleFrontendExit!\n");
      if(fFeEquipments.size()){
         CoolEq *ceq = dynamic_cast<CoolEq*>(fFeEquipments[0]);
         if(ceq){
            fMfe->Msg(MINFO, "HandleFrontendExit", "Venting vacuum vessel for 5s for safety");
            ceq->Vent(5);
         }
      }
   };
};

// boilerplate main function

int main(int argc, char* argv[])
{
   FeCooling fe_everything;
   return fe_everything.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
