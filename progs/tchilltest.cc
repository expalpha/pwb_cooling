#include <iostream>
#include "ThermoChill.hh"

using std::cout;
using std::cerr;
using std::endl;

int main(){
  ThermoChill tc;
  if(tc.CheckStatus()==0){
    cout << "Connected OK!" << endl;
  } else {
    cerr << "Error!" << endl;
    return 1;
  }
  
  double t = tc.ReadTemp();
  std::cout << "Temperature: " << t << std::endl;
  
  if(tc.SetTemp(17)){
    std::cout << "Successfully set SetPoint" << std::endl;
  } else {
    std::cerr << "Trouble changing SetPoint" << std::endl;
  }
  
  return 0;
}
